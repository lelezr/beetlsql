package org.beetl.sql.saga.demo.mapper;

import org.beetl.sql.saga.common.SagaMapper;
import org.beetl.sql.saga.demo.entity.OrderEntity;

public interface OrderMapper  extends SagaMapper<OrderEntity> {
}
